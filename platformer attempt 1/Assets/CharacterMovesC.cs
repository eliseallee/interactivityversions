﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovesC : MonoBehaviour {
    public float speed = 100;

    public float jump =1.0f;

    private PolygonCollider2D collide;

    private Rigidbody2D rb;

    void Start() {
    rb = GetComponent<Rigidbody2D>();
}


void Update() {
    collide = GetComponent<PolygonCollider2D>();

    float rbvelocity = Input.GetAxis("Horizontal") * speed * Time.deltaTime;
    Vector2 movement = new Vector2 (rbvelocity, rb.velocity.y);
rb.velocity = movement;

Vector3 max = collide.bounds.max;
Vector3 min = collide.bounds.min;
Vector2 corner1 = new Vector2(max.x, min.y - .1f);
Vector2 corner2 = new Vector2(min.x, min.y - .1f);
Collider2D hit = Physics2D.OverlapArea(corner1, corner2);

bool grounded = false;
if (hit != null) {
grounded = true;
}
if (grounded && Input.GetKeyDown(KeyCode.Z)) {
rb.AddForce(Vector2.up * jump, ForceMode2D.Impulse);
}
    }
    }
    
    